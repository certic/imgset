# certic-imgset

Web component vanilla pour l'affichage d'une image IIIF dans une page web.

## Installation

Copier le fichier le fichier certic-imgset.js dans votre web root

## Utilisation

Voir index.html pour un exemple d'utilisation.
