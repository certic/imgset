class CerticImgSet extends HTMLElement {
    constructor() {
        super();
        //let manifest = this.getAttribute("manifest");
        //this.urlRoot = manifest.substring(0, manifest.length - ("infos.json".length - 1));
        //this.alt = this.getAttribute("alt");
    }
    render() {
        let manifest = this.getAttribute("manifest");
        let urlRoot = manifest.substring(0, manifest.length - ("infos.json".length - 1));
        let alt = this.getAttribute("alt");
        /* First breakpoints follow Bootstrap 5 and then extends them for bigger pictures */
        this.innerHTML = `
            <picture>
                <source media="(max-width: 575px)" srcset="${urlRoot}full/^575,/0/default.jpg" />
                <source media="(min-width: 576px) and (max-width: 767px)" srcset="${urlRoot}full/^767,/0/default.jpg" />
                <source media="(min-width: 768px) and (max-width: 991px)" srcset="${urlRoot}full/^991,/0/default.jpg" />
                <source media="(min-width: 992px) and (max-width: 1199px)" srcset="${urlRoot}full/^1199,/0/default.jpg" />
                <source media="(min-width: 1200px) and (max-width: 1399px)" srcset="${urlRoot}full/^1399,/0/default.jpg" />
                <source media="(min-width: 1400px) and (max-width: 1999px)" srcset="${urlRoot}full/^1999,/0/default.jpg" />
                <source media="(min-width: 2000px) and (max-width: 2999px)" srcset="${urlRoot}full/^2999,/0/default.jpg" />
                <source media="(min-width: 3000px) and (max-width: 3999px)" srcset="${urlRoot}full/^3999,/0/default.jpg" />
                <source media="(min-width: 4000px) and (max-width: 4999px)" srcset="${urlRoot}full/^4999,/0/default.jpg" />
                <source media="(min-width: 5000px)" srcset="${urlRoot}full/^5000,/0/default.jpg" />
                <img src="${urlRoot}full/^575,/0/default.jpg" alt="${alt}" loading="lazy"/>
            </picture>`;

    }
    connectedCallback() {
        if (!this.rendered) {
            this.render();
            this.rendered = true;
        }
    }

}


customElements.define("certic-imgset", CerticImgSet);


export function replaceStandardImages(){
    let images = Array.from(document.getElementsByClassName("replace_by_set"));
    for(let i in images){
        let imageSet = document.createElement("certic-imgset");
        imageSet.setAttribute("alt", images[i].getAttribute("alt"));
        imageSet.setAttribute("manifest", images[i].getAttribute("src").split("/full/")[0] + "/info.json");
        images[i].parentNode.replaceChild(imageSet, images[i]);
    }
}